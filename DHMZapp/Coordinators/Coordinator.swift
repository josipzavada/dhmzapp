//
//  Coordinator.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

import Foundation
import UIKit
import RxSwift

class Coordinator {
    private let disposeBag = DisposeBag()
    
    private let window: UIWindow
    private var homeViewController: UIViewController?
    private var homeViewModel: HomeViewModelProtocol?
    private var navigationController: UINavigationController?
    
    init(window: UIWindow) {
        self.window = window
        self.window.makeKeyAndVisible()
    }
    
    func startApp() {
        let networkService = SevenDaysNetworkService()
        homeViewModel = HomeViewModel(networkService: networkService)
        guard let homeViewModel = homeViewModel else { return }
        homeViewController = HomeViewController(viewModel: homeViewModel)
        guard let homeViewController = homeViewController else { return }
        navigationController = UINavigationController(rootViewController: homeViewController)
        window.rootViewController = navigationController
        
        homeViewModel.addButtonTaps
            .subscribe(onNext: { [unowned self] in
                guard let cities = homeViewModel.cities else { return }
                self.presentSearchVC(with: cities)
            })
            .disposed(by: disposeBag)
        
        homeViewModel.cityCellTaps
            .subscribe(onNext: { [unowned self] city in
                self.pushSevenDaysVC(with: city)
            })
            .disposed(by: disposeBag)
    }
    
    private func presentSearchVC(with cities: [City]) {
        let searchViewController = SearchViewController(cities: cities)
        navigationController?.present(
            UINavigationController(rootViewController: searchViewController),
            animated: true
        )
        
        guard let homeViewModel = homeViewModel else { return }
        searchViewController.selectedCity
            .bind(to: homeViewModel.addedCity)
            .disposed(by: disposeBag)
    }
    
    private func pushSevenDaysVC(with city: City) {
        let sevenDaysVC = SevenDaysViewController(city: city)
        navigationController?.pushViewController(sevenDaysVC, animated: true)
    }
}
