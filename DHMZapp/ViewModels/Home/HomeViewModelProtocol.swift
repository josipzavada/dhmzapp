//
//  HomeViewModelProtocol.swift
//  DHMZapp
//
//  Created by Josip Zavada on 13.06.2021..
//

import RxSwift
import RxCocoa

protocol HomeViewModelProtocol {
    var cities: [City]? { get }
    var savedCityNames: [String] { get }
    var savedCities: [City]? { get }
    
    // Input
    var refreshTrigger: PublishSubject<Void> { get }
    var addButtonTaps: PublishSubject<Void> { get }
    var addedCity: PublishSubject<City> { get }
    var cityCellTaps: PublishSubject<City> { get }
    
    // Output
    var sevenDaysForecastRequest: Driver<SevenDaysServerResult>! { get }
    
    func removeCity(at index: Int)
}
