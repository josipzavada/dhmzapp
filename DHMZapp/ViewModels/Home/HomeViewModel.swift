//
//  HomeViewModel.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

import RxSwift
import RxCocoa
import Foundation

class HomeViewModel: HomeViewModelProtocol {
    private let disposeBag = DisposeBag()
    private let networkService: SevenDaysNetworkProtocol
    
    public var cities: [City]?
    public var savedCityNames: [String]
    public var savedCities: [City]?
    
    // Input
    public var refreshTrigger = PublishSubject<Void>()
    public var addButtonTaps = PublishSubject<Void>()
    public var addedCity = PublishSubject<City>()
    public var cityCellTaps = PublishSubject<City>()
    
    // Output
    public var sevenDaysForecastRequest: Driver<SevenDaysServerResult>!
    
    init(networkService: SevenDaysNetworkProtocol) {
        self.networkService = networkService
        let savedCityNamesFromDefaults = UserDefaults.standard.object(forKey: "savedCities") as? [String]
        savedCityNames = savedCityNamesFromDefaults ?? []
        setUpObservables()
    }
    
    private func setUpObservables() {
        sevenDaysForecastRequest = refreshTrigger
            .startWith(())
            .flatMap({ [unowned self] _ -> Observable<SevenDaysServerResult> in
                return self.networkService
                    .requestForecast()
            })
            .do(onNext: { [unowned self] response in
                self.handleResponse(response)
            })
            .asDriver(onErrorJustReturn: SevenDaysServerResult.error(DHMZAppError.unKnown))
        
        addedCity
            .subscribe( onNext: { [unowned self] city in
                self.handleAddedCity(city: city)
            })
            .disposed(by: disposeBag)
    }
    
    private func handleResponse(_ response: SevenDaysServerResult) {
        switch response {
        case .success(let cities):
            self.cities = cities
            self.savedCities = cities.filter({ city in
                return self.savedCityNames.contains(city.name)
            })
        default:
            break
        }
    }
    
    private func handleAddedCity(city: City) {
        guard !self.savedCityNames.contains(city.name) else { return }
        self.savedCities?.append(city)
        self.savedCityNames.append(city.name)
        let userDefaults = UserDefaults.standard
        userDefaults.set(self.savedCityNames, forKey: "savedCities")
    }
    
    public func removeCity(at index: Int) {
        guard let city = savedCities?[safe: index] else { return }
        savedCities?.removeAll { savedCity in
            city.name == savedCity.name
        }
        savedCityNames.removeAll { savedCityName in
            city.name == savedCityName
        }
        UserDefaults.standard.set(savedCityNames, forKey: "savedCities")
    }
}
