//
//  DHMZNetworkRequestService.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

import Foundation
import RxSwift
import RxCocoa

class DHMZNetworkRequestService {
    private let baseURL = URL(string: "https://prognoza.hr/")!

    func send(apiEndpoint: APIEndpoint) -> Observable<Data> {
        let request = apiEndpoint.request(with: baseURL)
        return URLSession.shared.rx.data(request: request)
            .observe(on: MainScheduler.asyncInstance)
    }
}
