//
//  RequestType.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

public enum RequestType: String {
    case GET, POST
}
