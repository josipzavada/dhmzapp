//
//  SevenDaysForecastEndpoint.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

class SevenDaysForecastEndpoint: APIEndpoint {
    var method: RequestType = .GET
    var path = "sedam/hrvatska/7d_meteogrami.xml"
    var parameters: [String : String] = [:]
}
