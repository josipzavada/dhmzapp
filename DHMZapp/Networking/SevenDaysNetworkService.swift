//
//  SevenDaysNetworkService.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

import Foundation
import RxSwift
import RxCocoa

protocol SevenDaysNetworkProtocol {
    func requestForecast() -> Observable<SevenDaysServerResult>
}

class SevenDaysNetworkService: SevenDaysNetworkProtocol {
    let requestService = DHMZNetworkRequestService()
    
    func requestForecast() -> Observable<SevenDaysServerResult> {
        let sevenDaysEndpoint = SevenDaysForecastEndpoint()
        return requestService
            .send(apiEndpoint: sevenDaysEndpoint)
            .mapToSevenDaysData()
            .map({ cities in
                return .success(cities)
            })
            .catch { error in
                return Observable.just(SevenDaysServerResult.error(error))
            }
    }
}

private extension ObservableType where Element == Data {
    func mapToSevenDaysData() -> Observable<[City]> {
        return map { data in
            return try SevenDaysForecastParser().parse(data: data)
        }
    }
}
