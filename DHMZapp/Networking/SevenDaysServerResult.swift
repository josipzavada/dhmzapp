//
//  SevenDaysServerResult.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

enum SevenDaysServerResult {
    case success([City])
    case error(Error)
}
