//
//  AppDelegate.swift
//  DHMZapp
//
//  Created by Josip Zavada on 28.05.2021..
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    private var coordinator: Coordinator!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let newWindow = UIWindow(frame: UIScreen.main.bounds)
        window = newWindow
        coordinator = Coordinator(window: newWindow)
        coordinator.startApp()
        return true
    }
}
