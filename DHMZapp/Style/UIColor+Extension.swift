//
//  UIColor+Extension.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

import UIKit

public extension UIColor {
    static let background = UIColor(named: "background")
    static let foreground = UIColor(named: "foreground")
}
