//
//  StyleGuide.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

import UIKit

class StyleGuide {
    static let unit = CGFloat(8.0)
    static let doubleUnit = unit * 2
    static let tripleUnit = unit * 3
}
