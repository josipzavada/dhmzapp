//
//  HomeViewController.swift
//  DHMZapp
//
//  Created by Josip Zavada on 28.05.2021..
//

import UIKit
import RxSwift
import SnapKit

class HomeViewController: BaseViewController {
    private let viewModel: HomeViewModelProtocol
    private let disposeBag = DisposeBag()
    
    private let tableView = UITableView()
    private let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: nil)
    private let emptyStateLabel = UILabel()
    
    init(viewModel: HomeViewModelProtocol) {
        self.viewModel = viewModel
        super.init()
        view.backgroundColor = .background
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpObservables()
        renderUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigationBar()
    }
    
    private func setUpObservables() {
        viewModel
            .sevenDaysForecastRequest
            .drive(onNext: { [unowned self] response in
                switch response {
                case .success(_):
                    self.setUp()
                case .error(let error):
                    self.handleError(error)
                }
            })
            .disposed(by: disposeBag)
        
        viewModel.addedCity
            .subscribe(onNext: { [unowned self] _ in
                self.tableView.reloadData()
                hideOrShowEmptyState()
            })
            .disposed(by: disposeBag)
        
        addButton.rx.tap
            .bind(to: viewModel.addButtonTaps)
            .disposed(by: disposeBag)
    }
    
    private func setUpNavigationBar() {
        title = "DHMZ 7 dana"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = addButton
    }
    
    private func setUp() {
        hideOrShowEmptyState()
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    private func hideOrShowEmptyState() {
        if viewModel.savedCityNames.isEmpty {
            emptyStateLabel.isHidden = false
        } else {
            emptyStateLabel.isHidden = true
        }
    }
    
    private func renderUI() {
        tableView.register(CityCell.self, forCellReuseIdentifier: "CityCell")
        tableView.backgroundColor = .background
        tableView.delegate = self
        tableView.separatorStyle = .none
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        emptyStateLabel.text = "Nemate dodanih gradova.\nPritisnite + da biste dodali grad."
        emptyStateLabel.numberOfLines = 0
        emptyStateLabel.textAlignment = .center
        emptyStateLabel.textColor = .gray
        tableView.addSubview(emptyStateLabel)
        emptyStateLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(200)
            make.leading.trailing.equalToSuperview().inset(StyleGuide.tripleUnit)
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.savedCities?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell") as? CityCell
        else { return UITableViewCell() }
        cell.selectionStyle = .none
        guard let city = viewModel.savedCities?[indexPath.row]
        else { return UITableViewCell() }
        cell.setUp(with: city)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let city = viewModel.savedCities?[indexPath.row] else { return }
        viewModel.cityCellTaps.onNext(city)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.removeCity(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            hideOrShowEmptyState()
        }
    }
}
