//
//  CityCell.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

import UIKit
import SnapKit

class CityCell: UITableViewCell {
    private let cityLabel = UILabel()
    private let iconImageView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        renderUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setUp(with city: City) {
        cityLabel.text = city.name
        iconImageView.image = UIImage(named: city.days[0].hours[0].symbol)
    }
    
    private func renderUI() {
        let containerView = UIView()
        containerView.layer.cornerRadius = StyleGuide.unit
        containerView.backgroundColor = .foreground
        contentView.backgroundColor = .background
        contentView.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(StyleGuide.doubleUnit)
            make.top.bottom.equalToSuperview().inset(StyleGuide.unit)
        }
        
        containerView.addSubview(iconImageView)
        iconImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(StyleGuide.tripleUnit)
            make.height.width.equalTo(StyleGuide.tripleUnit)
            make.centerY.equalToSuperview()
        }
        
        containerView.addSubview(cityLabel)
        cityLabel.snp.makeConstraints { make in
            make.leading.equalTo(iconImageView.snp.trailing).offset(StyleGuide.doubleUnit)
            make.top.equalToSuperview().offset(StyleGuide.tripleUnit)
            make.bottom.equalToSuperview().inset(StyleGuide.tripleUnit)
        }
        
        let chevronImageView = UIImageView()
        chevronImageView.image = UIImage(named: "rightChevron")
        containerView.addSubview(chevronImageView)
        chevronImageView.snp.makeConstraints { make in
            make.width.height.equalTo(StyleGuide.doubleUnit)
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(StyleGuide.tripleUnit)
        }
    }
}
