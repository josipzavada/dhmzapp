//
//  SearchViewController.swift
//  DHMZapp
//
//  Created by Josip Zavada on 30.05.2021..
//

import UIKit
import RxSwift

class SearchViewController: BaseViewController {
    private let cities: [City]
    private var filteredCities: [City]
    
    private let searchController = UISearchController()
    private let tableView = UITableView()
    
    public let selectedCity = PublishSubject<City>()
    
    init(cities: [City]){
        self.cities = cities
        self.filteredCities = cities
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Dodaj grad"
        renderUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationItem.hidesSearchBarWhenScrolling = true
    }
    
    private func setUpNavigationBar() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        navigationItem.searchController = searchController
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.sizeToFit()
        navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    private func isFiltering() -> Bool {
        let numberOfLetters = searchController.searchBar.text?.count ?? 0
        return numberOfLetters > 0
    }
    
    private func renderUI() {
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

// MARK: TableView delegates
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredCities.count
        } else {
            return cities.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell")
        else { return UITableViewCell() }
        if isFiltering() {
            cell.textLabel?.text = filteredCities[indexPath.row].name
        } else {
            cell.textLabel?.text = cities[indexPath.row].name
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFiltering() {
            selectedCity.onNext(filteredCities[indexPath.row])
        } else {
            selectedCity.onNext(cities[indexPath.row])
        }
        self.navigationController?.dismiss(animated: true)
    }
    
}

// MARK: SearchController delegates
extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let queryString = searchController.searchBar.text!
        filteredCities = cities.filter({ city in
            return city.name.lowercased().contains(queryString.lowercased())
        })
        tableView.reloadData()
    }
}
