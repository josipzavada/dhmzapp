//
//  HourView.swift
//  DHMZapp
//
//  Created by Josip Zavada on 01.06.2021..
//

import UIKit

class HourView: UIView {
    private let stackView = UIStackView()
    
    private let hourLabel = UILabel()
    private let weatherSymbolImageView = UIImageView()
    private let windSymbolImageView = UIImageView()
    private let temperatureLabel = UILabel()
    private let precipitationLabel = UILabel()
    
    init() {
        super.init(frame: .zero)
        renderUI()
    }
    
    public func setUp(with hour: Hour?) {
        guard let hour = hour else {
            isHidden = true
            return
        }
        isHidden = false
        hourLabel.text = "\(hour.hour):00"
        temperatureLabel.text = "\(hour.temperature) ºC"
        precipitationLabel.text = "\(hour.precipitation) mm"
        guard hour.symbol != "", hour.wind != "" else { return }
        weatherSymbolImageView.image = UIImage(named: hour.symbol)
        windSymbolImageView.image = UIImage(named: hour.wind)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func renderUI() {
        stackView.axis = .horizontal
        stackView.distribution = .equalCentering
        addSubview(stackView)
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        weatherSymbolImageView.snp.makeConstraints { make in
            make.width.height.lessThanOrEqualTo(40)
        }
        
        windSymbolImageView.snp.makeConstraints { make in
            make.width.height.lessThanOrEqualTo(40)
        }
        
        stackView.addArrangedSubview(hourLabel)
        stackView.addArrangedSubview(weatherSymbolImageView)
        stackView.addArrangedSubview(windSymbolImageView)
        stackView.addArrangedSubview(temperatureLabel)
        stackView.addArrangedSubview(precipitationLabel)
    }
    
}
