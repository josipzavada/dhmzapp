//
//  SevenDaysViewController.swift
//  DHMZapp
//
//  Created by Josip Zavada on 31.05.2021..
//

import UIKit
import SnapKit

class SevenDaysViewController: BaseViewController {
    private let tableView = UITableView()
    private let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: nil)
    
    private let city: City
    
    init(city: City) {
        self.city = city
        super.init()
        title = city.name
        view.backgroundColor = .background
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpObservables()
        renderUI()
    }
    
    private func setUpObservables() {
        
    }
    
    private func renderUI() {
        tableView.register(CityCell.self, forCellReuseIdentifier: "CityCell")
        tableView.backgroundColor = .background
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.register(DayCell.self, forCellReuseIdentifier: "DayCell")
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

extension SevenDaysViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return city.days.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DayCell") as? DayCell
        else { return UITableViewCell() }
        guard let day = city.days[safe: indexPath.row] else { return UITableViewCell() }
        cell.setUp(with: day)
        return cell
    }
}
