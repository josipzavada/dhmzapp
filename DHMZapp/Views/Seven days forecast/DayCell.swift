//
//  DayCell.swift
//  DHMZapp
//
//  Created by Josip Zavada on 01.06.2021..
//

import UIKit

class DayCell: UITableViewCell {
    private let dayLabel = UILabel()
    
    private let hourViews = [
        HourView(),
        HourView(),
        HourView(),
        HourView(),
        HourView(),
        HourView(),
        HourView(),
        HourView()
    ]
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        renderUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setUp(with day: Day) {
        dayLabel.text = day.name
        hourViews.enumerated().forEach { (index, hourView) in
            let hour = day.hours[safe: index]
            hourView.setUp(with: hour)
        }
    }
    
    private func renderUI() {
        let containerView = UIView()
        containerView.layer.cornerRadius = StyleGuide.unit
        containerView.backgroundColor = .foreground
        contentView.backgroundColor = .background
        contentView.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(StyleGuide.doubleUnit)
        }
        
        dayLabel.text = "Ponedjeljak"
        dayLabel.font = .boldSystemFont(ofSize: 25)
        containerView.addSubview(dayLabel)
        dayLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(StyleGuide.doubleUnit)
            make.leading.equalToSuperview().offset(StyleGuide.doubleUnit)
        }
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 10
        containerView.addSubview(stackView)
        stackView.snp.makeConstraints { make in
            make.top.equalTo(dayLabel.snp.bottom).offset(StyleGuide.tripleUnit)
            make.leading.trailing.equalToSuperview().inset(StyleGuide.tripleUnit)
            make.bottom.equalToSuperview().inset(StyleGuide.unit)
        }
        
        hourViews.forEach { hourView in
            stackView.addArrangedSubview(hourView)
        }
    }
}
