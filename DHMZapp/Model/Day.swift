//
//  Day.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

struct Day {
    let date: String
    let name: String
    var hours: [Hour]
}
