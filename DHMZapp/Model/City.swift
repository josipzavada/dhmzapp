//
//  City.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

struct City {
    let name: String
    let location: String
    let code: String
    let days: [Day]
}
