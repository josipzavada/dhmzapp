//
//  DHMZAppError.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

import Foundation

enum DHMZAppError: Error {
    case parsingFailed
    case unKnown
}

extension DHMZAppError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .unKnown:
            return NSLocalizedString("An unknown error has occurred", comment: "Unknown error")
        case .parsingFailed:
            return NSLocalizedString("Received data could not be decoded", comment: "Decoding failed")
        }
    }
}
