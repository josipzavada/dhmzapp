//
//  Hour.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

struct Hour {
    let hour: String
    let temperature: String
    let symbol: String
    let wind: String
    let precipitation: String
}
