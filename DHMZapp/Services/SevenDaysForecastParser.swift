//
//  SevenDaysForecastParser.swift
//  DHMZapp
//
//  Created by Josip Zavada on 29.05.2021..
//

import Foundation

class SevenDaysForecastParser: NSObject {
    private var currentElement = String()
    
    private var cityName = String()
    private var cityLocation = String()
    private var cityCode = String()
    
    private var date = String()
    private var weekDay = String()
    private var hour = String()
    
    private var temperature = String()
    private var symbol = String()
    private var wind = String()
    private var precipitation = String()
    
    private var cities: [City] = []
    private var days: [Day] = []
    
    private var error: Error?
    
    func parse(data: Data) throws -> [City] {
        let parser = XMLParser(data: data)
        parser.delegate = self
        parser.parse()
        guard let error = error else { return cities }
        throw error
    }
}

// MARK: Delegate functions
extension SevenDaysForecastParser: XMLParserDelegate {
    func parser(
        _ parser: XMLParser,
        didStartElement elementName: String,
        namespaceURI: String?,
        qualifiedName qName: String?,
        attributes attributeDict: [String : String] = [:]
    ) {
        
        switch elementName {
        case "grad":
            setUpCityVariables(attributeDict: attributeDict, parser: parser)
        case "dan":
            setUpDayVariables(attributeDict: attributeDict, parser: parser)
        default:
            break
        }
        currentElement = elementName
    }
    
    func parser(
        _ parser: XMLParser,
        didEndElement elementName: String,
        namespaceURI: String?,
        qualifiedName qName: String?
    ) {
        
        switch elementName {
        case "dan":
            saveDay()
        case "grad":
            saveCity()
        default:
            break
        }
    }
    
    func parser(
        _ parser: XMLParser,
        foundCharacters string: String
    ) {
        
        let text = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if !text.isEmpty {
            switch currentElement {
            case "t_2m":
                temperature += text
            case "simbol":
                symbol += text
            case "vjetar":
                wind += text
            case "oborina":
                precipitation += text
            default:
                break
            }
        }
    }
}

// MARK: Helper functions
private extension SevenDaysForecastParser {
    func appendHour(hour: Hour, weekDay: String, date: String) {
        let dayIndex = days.firstIndex { day in
            return day.name == weekDay
        }
        
        if dayIndex == nil {
            let dayToAppend = Day(date: date, name: weekDay, hours: [hour])
            days.append(dayToAppend)
        } else {
            guard let dayIndex = dayIndex else { return }
            days[dayIndex].hours.append(hour)
        }
    }
    
    func setUpCityVariables(attributeDict: [String : String], parser: XMLParser) {
        guard let cityNameUnwrapped = attributeDict["ime"],
              let cityLocationUnwrapped = attributeDict["lokacija"],
              let cityCodeUnwrapped = attributeDict["code"]
        else {
            parser.abortParsing()
            error = DHMZAppError.parsingFailed
            return
        }
        cityName = cityNameUnwrapped
        cityLocation = cityLocationUnwrapped
        cityCode = cityCodeUnwrapped
    }
    
    func setUpDayVariables(attributeDict: [String : String], parser: XMLParser) {
        guard let dateUnwrapped = attributeDict["datum"],
              let weekdayUnwrapped = attributeDict["dtj"],
              let hourUnwrapped = attributeDict["sat"]
        else {
            parser.abortParsing()
            error = DHMZAppError.parsingFailed
            return
        }
        date = dateUnwrapped
        weekDay = weekdayUnwrapped
        hour = hourUnwrapped
    }
    
    func saveDay() {
        let hour = Hour(
            hour: hour,
            temperature: temperature,
            symbol: symbol,
            wind: wind,
            precipitation: precipitation
        )
        appendHour(hour: hour, weekDay: weekDay, date: date)
        
        temperature = ""
        symbol = ""
        wind = ""
        precipitation = ""
    }
    
    func saveCity() {
        let city = City(
            name: cityName,
            location: cityLocation,
            code: cityCode,
            days: days
        )
        cities.append(city)
        days = []
    }
}
