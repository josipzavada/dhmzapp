//
//  Collection+Extension.swift
//  DHMZapp
//
//  Created by Josip Zavada on 01.06.2021..
//

extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
